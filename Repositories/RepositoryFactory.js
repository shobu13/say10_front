import OrdersRepository from './OrdersRepository'

const repositories = {
  orders: OrdersRepository
}

export const RepositoryFactory = {
  get: name => repositories[name]
}
