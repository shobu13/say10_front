import Repository from './Repository'

export default {
  get() {
    return Repository.gitkraken.get('')
  },
  getOrder(orderId) {
    return Repository.gitkraken.get('')
  },
  postOrder(payload) {
    return Repository.gitkraken.post('/cards', payload)
  }
}
