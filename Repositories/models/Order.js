class Order {
  constructor({ name = '', position = 0, description = { text: '' }, column_id, assignees = undefined, labels = [], due_date = new Date }) {
    this.name = name
    this.position = position
    this.description = description
    this.column_id = column_id
    this.assignees = assignees
    this.labels = labels
    this.due_date = due_date
  }

  static buildDescription(item_id, item_name, sizes, address, comments) {
    return `
product: ${item_id}
name: ${item_name}

---

sizes:
  bust: ${sizes.bust} cm
  waist: ${sizes.waist} cm
  hips: ${sizes.hips} cm

---

comments:

${comments}

---

${address.name}
${address.street}
${address.zip}, ${address.city}

${address.mail}
${address.phone}
`
  }
}

export default Order
