import axios from 'axios'
import firebase from 'firebase/app'
import 'firebase/firestore'

const gitkrakenDomain = 'https://gloapi.gitkraken.com'
const baseURL = `${gitkrakenDomain}/v1/glo/boards/${process.env.GITKRAKEN_BOARD_ID}`

export default {
  gitkraken: axios.create({
    baseURL,
    headers: { 'Authorization': `Bearer ${process.env.GITKRAKEN_API_KEY}` }
  }),
  /*firebase: firebase.initializeApp({
    apiKey: process.env.FIREBASE_API_KEY,
    authDomain: process.env.FIREBASE_AUTH_DOMAIN,
    projectId: process.env.FIREBASE_PROJECT_ID,
    storageBucket: process.env.FIREBASE_STORAGE_BUCKET,
    messagingSenderId: process.env.FIREBASE_MESSAGING_SENDER_ID,
    appId: process.env.FIREBASE_APP_ID
  })*/
}
